This is a utility for reading the output of [Ripser](https://github.com/Ripser/ripser) into Numpy.
It is also compatible with the output of [Flagser](https://github.com/luetge/flagser).

## Installation

The project can be installed directly from the Git respository using pip (or your dependency manager of choice):

```
pip install git+https://gitlab.com/kfbenjamin/readripser.git
```

For development purposes, the package is maintained with Poetry. So you can
alternatively install Poetry and then run ```poetry install``` in the cloned repository.

## Usage

Usage is very simple. If you store your Ripser output in ```foo.txt``` then running
```python
import readripser

readripser.read_ripser_file('foo.txt')
```
will output a list of Numpy arrays, the i-th of which corresponds to the persistence diagram in dimension i.

## Acknowledgements

The regular expression approach taken in the package is inspired by the ```ripser_reformat_output.m``` Matlab script due to Parker Edwards, which is bundled in [Nina Otter's repository](https://github.com/n-otter/PH-roadmap). 
