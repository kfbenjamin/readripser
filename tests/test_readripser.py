import string
from math import inf

import hypothesis.strategies as st
from hypothesis import given

from readripser import __version__, get_endpoints


whitespace = st.text(string.whitespace)


def test_version():
    assert __version__ == '0.3.2'


@given(st.floats(allow_nan=False, allow_infinity=False),
       st.floats(allow_nan=False, allow_infinity=False),
       whitespace,
       whitespace
       )
def test_endpoints_finite(birth, persistence, start_padding, end_padding):
    line = start_padding + f"[{birth}, {birth + persistence})" + end_padding
    endpoints = get_endpoints(line)
    assert endpoints == (birth, birth+persistence)


@given(st.floats(allow_nan=False, allow_infinity=False), whitespace, whitespace)
def test_endpoints_infinite(birth, start_padding, end_padding):
    line = start_padding + f" [{birth}, )" + end_padding
    endpoints = get_endpoints(line)
    assert endpoints == (birth, inf)
