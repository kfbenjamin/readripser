"""Reads Ripser files into Numpy arrays."""
import re
from typing import Iterable

import numpy as np
import numpy.typing as npt

__version__ = "0.3.2"


def get_endpoints(line: str) -> tuple[float, float]:
    """Retrieves the birth and death times corresponding to a Ripser line.

    Args:
        A string of the form '[x,y)' or '[x, )', possibly with some
        leading or trailing whitespace.

    Returns:
        In the first case, the pair (x, y). Otherwise, the pair (x, inf).
    """
    line = re.sub(r"\[", "", line)
    line = re.sub(r",\ \)", " inf", line)
    line = re.sub(r",\ ", " ", line)
    line = re.sub(r",", " ", line)
    line = re.sub(r"\)", "", line)
    return tuple(map(float, line.strip().split(' ')))


def read_ripser(lines: Iterable[str]) -> list[npt.NDArray]:
    """Converts Ripser output into a list of persistence diagrams.

    Args:
        lines:
            A sequence of strings, each representing a line in Ripser output.

    Returns:
        A list of Numpy arrays. The i-th such array is the persistence diagram
        in degree i that the Ripser lines encode.
    """
    dim = -1
    diagrams = []
    diagram = []
    for line in lines:
        if re.search(r"persistence", line):
            dim += 1
            if dim > 0:
                diagrams.append(np.array(diagram))
                diagram = []

        elif re.search(r"\[.*\)", line):
            diagram.append(get_endpoints(line))

    if dim > -1:
        diagrams.append(np.array(diagram))

    return diagrams


def read_ripser_file(filename: str) -> list[npt.NDArray]:
    """Converts a Ripser file into a list of persistence diagrams.

    Args:
        filename: Path to a file containing ripser output.

    Returns:
        A list of Numpy arrays. The i-th such array is the persistence diagram
        in degree i that the Ripser file encodes.
    """
    with open(filename, 'r', encoding='utf8') as file:
        lines = [line.rstrip() for line in file.readlines()]
    return read_ripser(lines)
